package com.timby.vknotes.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by andre on 07.09.17.
 */

public class UIUtils {

    private static final int HIDE_KEYBOARD_DELAY = 300;

    public static void hideKeyboard(View view, Runnable afterHide) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.hideSoftInputFromWindow(view.getWindowToken(), 0)) {
            if (afterHide != null)
                new Handler().postDelayed(afterHide, HIDE_KEYBOARD_DELAY);
        } else if (afterHide != null) afterHide.run();
    }

    public static void showSoftKeyboard(View view, Runnable afterHide) {
        InputMethodManager keyboard = (InputMethodManager)
                view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (keyboard.showSoftInput(view, 0)) {
            if (afterHide != null)
                new Handler().postDelayed(afterHide, HIDE_KEYBOARD_DELAY);
        } else if (afterHide != null) afterHide.run();
    }

    public static void hideKeyboard(Activity activity, Runnable afterHide) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm.hideSoftInputFromWindow(view.getWindowToken(), 0)) {
            if (afterHide != null)
                new Handler().postDelayed(afterHide, HIDE_KEYBOARD_DELAY);
        } else if (afterHide != null) afterHide.run();
    }

    public static int getWindowHeight(Activity activity){
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int dpToPx(Context context, float dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static float pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }


}
