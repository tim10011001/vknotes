package com.timby.vknotes.utils;

import android.content.Context;

import java.io.File;
import java.io.IOException;

/**
 * Created by andre on 08.09.17.
 */

public class AssetsUtils {

    public static final String STICKERS_ASSETS_FOLDER = "stickers";

    public static String[] getAssetsInFolder(Context context, String folder) {
        String[] fileNames = new String[0];
        try {
            fileNames = context.getAssets().list(folder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < fileNames.length; i++) {
            fileNames[i] = folder + File.separator + fileNames[i];
        }

        return fileNames;
    }

}
