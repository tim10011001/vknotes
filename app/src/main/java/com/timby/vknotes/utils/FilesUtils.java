package com.timby.vknotes.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andre on 10.09.17.
 */

public class FilesUtils {

    public static String getImagePath(Context context, int imageId) {
        Cursor cursor;
        cursor = context.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.MediaColumns._ID, MediaStore.MediaColumns.DATA},
                MediaStore.MediaColumns._ID + " = " + imageId,
                null, null);

        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        if (cursor.moveToNext()) {
            String filePath = cursor.getString(column_index_data);
            cursor.close();
            return filePath;
        }


        return null;
    }

    public static String getImageThumbPath(Context context, int imageId) {
        Cursor cursor;
        cursor = context.getContentResolver().query(
                android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Thumbnails.IMAGE_ID, MediaStore.MediaColumns.DATA},
                MediaStore.Images.Thumbnails.IMAGE_ID + " = " + imageId,
                null, null);

        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        if (cursor.moveToNext()) {
            String filePath = cursor.getString(column_index_data);
            cursor.close();
            return filePath;
        }

        return null;
    }


    public static List<Integer> getImages(Context context) {
        Cursor cursor;
        cursor = context.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.MediaColumns._ID, MediaStore.MediaColumns.DATA},
                null,
                null,
                MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        List<Integer> ids = new ArrayList<>();
        int column_index_ID = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID);
        while (cursor.moveToNext()) {
            ids.add(cursor.getInt(column_index_ID));
        }
        cursor.close();

        return ids;
    }

    public static String getImageFilePath(Context context, Uri imageUri) {
        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(imageUri, filePath, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            cursor.close();
            return imagePath;
        }
        return null;
    }
}
