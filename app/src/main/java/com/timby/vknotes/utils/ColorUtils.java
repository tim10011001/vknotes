package com.timby.vknotes.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.View;

import com.timby.vknotes.R;

/**
 * Created by andre on 19.09.17.
 */

public class ColorUtils {

    public static int getAverageColor(View view) {
        Bitmap b = Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        view.draw(c);
        return getAverageColor(b);
    }

    public static int getAverageColor(Bitmap bitmap) {
        long redBucket = 0;
        long greenBucket = 0;
        long blueBucket = 0;
        long pixelCount = 0;

        for (int y = 0; y < bitmap.getHeight(); y++) {
            for (int x = 0; x < bitmap.getWidth(); x++) {
                int c = bitmap.getPixel(x, y);

                pixelCount++;
                redBucket += Color.red(c);
                greenBucket += Color.green(c);
                blueBucket += Color.blue(c);
                // does alpha matter?
            }
        }

        return Color.rgb((int) (redBucket / pixelCount),
                (int) (greenBucket / pixelCount),
                (int) (blueBucket / pixelCount));
    }

    public static int getContrastTextColor(Context context, int... colorBackGround) {
        long sumC = 0;

        for (int backColor : colorBackGround) {
            backColor *= (((backColor & 0xff000000) >> 24) & 0xff) / (float) 0xff;

            sumC += (((backColor & 0x00ff0000) >> 16) & 0xff)
                    + (((backColor & 0x0000ff00) >> 8) & 0xff)
                    + ((backColor & 0x000000ff) & 0xff);

            if (sumC > 0xFFFFFF) break;
        }


        if (sumC > 0x2c0)
//            background is a white
            return context.getResources().getColor(R.color.black);
        return context.getResources().getColor(R.color.white);
    }
}
