package com.timby.vknotes.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Random;

/**
 * Created by andre on 13.09.17.
 */

public class StickerPos implements Parcelable {

    private String stickerPath;
    /**
     * range (-1,+1)
     */
    private float x, y;
    /**
     * in degrees
     */
    private float angle = 0;
    private float scale = 1;

    public static StickerPos pasteToFreePlace(String stickerPath, List<StickerPos> stickerPoses) {
        Random random = new Random();

        return new StickerPos(stickerPath,
                random.nextFloat() * 1.6f - 0.8f,
                random.nextFloat() * 1.6f - 0.8f,
                random.nextFloat() * 45f,
                random.nextFloat() * 1.5f + 0.7f);
    }

    public StickerPos(String stickerPath, float x, float y) {
        this.stickerPath = stickerPath;
        this.x = x;
        this.y = y;
    }

    public StickerPos(String stickerPath, float x, float y, float angle, float scale) {
        this.stickerPath = stickerPath;
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.scale = scale;
    }


    private StickerPos(Parcel in) {
        stickerPath = in.readString();
        x = in.readFloat();
        y = in.readFloat();
        angle = in.readFloat();
        scale = in.readFloat();
    }

    public String getStickerPath() {
        return stickerPath;
    }

    public void setStickerPath(String stickerPath) {
        this.stickerPath = stickerPath;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stickerPath);
        dest.writeFloatArray(new float[]{x, y, angle, scale});
    }

    public static final Creator<StickerPos> CREATOR = new Creator<StickerPos>() {
        @Override
        public StickerPos createFromParcel(Parcel in) {
            return new StickerPos(in);
        }

        @Override
        public StickerPos[] newArray(int size) {
            return new StickerPos[size];
        }
    };
}
