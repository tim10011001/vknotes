package com.timby.vknotes.network;

import android.graphics.Bitmap;

/**
 * Created by tim on 10.09.17.
 */

public interface IPoster {
    void post(Bitmap bitmap, RequestCallback callback);

    interface RequestCallback{
        void onSuccess();
        void onError(Throwable exception);
    }
}
