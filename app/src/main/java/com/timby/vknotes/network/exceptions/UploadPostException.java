package com.timby.vknotes.network.exceptions;

/**
 * Created by tim on 11.09.17.
 */

public class UploadPostException extends Exception{

    public UploadPostException(String message) {
        super(message);
    }
}
