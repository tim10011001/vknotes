package com.timby.vknotes.network;

import android.graphics.Bitmap;
import android.util.Log;

import com.timby.vknotes.network.exceptions.UploadPostException;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.model.VKWallPostResult;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

/**
 * Created by tim on 10.09.17.
 */

public class Poster implements IPoster {

    private static final String TAG = Poster.class.getSimpleName();

    private int getAccountId(){
        final VKAccessToken vkAccessToken = VKAccessToken.currentToken();
        return vkAccessToken != null ? Integer.parseInt(vkAccessToken.userId) : 0;
    }

    @Override
    public void post(Bitmap bitmap, RequestCallback callback){
        VKRequest request = VKApi.uploadWallPhotoRequest(new VKUploadImage(bitmap, VKImageParameters.jpgImage(0.9f)), getAccountId(), 0);

        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKApiPhoto photoModel =  ((VKPhotoArray) response.parsedModel).get(0);
                makePost(new VKAttachments(photoModel), getAccountId());
                callback.onSuccess();
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
            }

            @Override
            public void onError(VKError error)
            {
                super.onError(error);
                callback.onError(new UploadPostException(error.errorMessage));
            }

            @Override
            public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
                super.onProgress(progressType, bytesLoaded, bytesTotal);
            }
        });
    }


    private void makePost(VKAttachments attachments, final int ownerId) {
        VKParameters params = new VKParameters();
        params.put(VKApiConst.OWNER_ID, ownerId);
        params.put(VKApiConst.ATTACHMENTS, attachments);

        VKRequest wallRequest = VKApi.wall().post(params);
        wallRequest.setModelClass(VKWallPostResult.class);

        wallRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                Log.d(TAG, "onComplete: Success");
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                Log.d(TAG, "onError: Error!");
            }

            @Override
            public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
                super.onProgress(progressType, bytesLoaded, bytesTotal);
            }
        });
    }
}
