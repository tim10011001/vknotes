package com.timby.vknotes.adapters.listeners;

/**
 * Created by andre on 18.09.17.
 */

public interface OnStickerPressedListener {
    void onStickerPressed(String stickerPath);
}
