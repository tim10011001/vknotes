package com.timby.vknotes.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.timby.vknotes.R;
import com.timby.vknotes.adapters.listeners.OnStickerPressedListener;

import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andre on 08.09.17.
 */

public class StickersAdapter extends RecyclerView.Adapter<StickersAdapter.StickerItem> {

    private String[] assetFilePaths;
    private Context context;
    private OnStickerPressedListener listener;

    public StickersAdapter(Context contexts, String[] assetFilePaths, OnStickerPressedListener listener) {
        this.context = contexts;
        this.assetFilePaths = assetFilePaths;
        this.listener = listener;
    }

    @Override
    public StickerItem onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StickerItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.sticker_item, parent, false));
    }

    @Override
    public void onBindViewHolder(StickerItem holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return assetFilePaths.length;
    }

    public class StickerItem extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_item)
        ImageView ivSticker;

        private String assetPath = null;

        public StickerItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void bind(int pos) {
            this.assetPath = assetFilePaths[pos];

            InputStream stream = null;
            try {
                stream = context.getAssets().open(this.assetPath);
                ivSticker.setImageBitmap(BitmapFactory.decodeStream(stream));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.iv_item)
        void onClick() {
            if (listener != null) {
                listener.onStickerPressed(this.assetPath);
            }
        }


    }

}
