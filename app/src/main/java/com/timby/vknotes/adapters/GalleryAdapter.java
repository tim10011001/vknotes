package com.timby.vknotes.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.timby.vknotes.R;
import com.timby.vknotes.adapters.listeners.OnGalleryClicked;
import com.timby.vknotes.utils.BitmapUtils;
import com.timby.vknotes.utils.FilesUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andre on 08.09.17.
 */

public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Integer> imageIds;
    private final Context context;

    private OnGalleryClicked onGalleryClicked;

    public GalleryAdapter(Context contexts, List<Integer> imageIds) {
        this.context = contexts;
        this.imageIds = imageIds;
    }

    public OnGalleryClicked getOnGalleryClicked() {
        return onGalleryClicked;
    }

    public void setOnGalleryClicked(OnGalleryClicked onGalleryClicked) {
        this.onGalleryClicked = onGalleryClicked;
    }

    @Override
    public int getItemCount() {
        return imageIds.size() + 2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new IconItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof IconItem)
            ((IconItem) holder).bind(position, position);
    }


    class IconItem extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_item)
        ImageView imageView;
        private int index;

        IconItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        void bind(int index, int pos) {
            this.index = index;
            if (index < 2) {
                switch (index) {
                    case 0:
                        imageView.setImageResource(R.drawable.ic_photopicker_albums);
                        break;
                    case 1:
                        imageView.setImageResource(R.drawable.ic_photopicker_camera);
                        break;
                }
                return;
            }

            String imageFile = FilesUtils.getImageThumbPath(context, imageIds.get(index - 2));
            if (imageFile != null)
                imageView.setImageBitmap(BitmapUtils.loadBitmap(imageFile));
            else
                imageView.setImageDrawable(new ColorDrawable(context.getResources().getColor(R.color.grey)));
        }

        @OnClick(R.id.iv_item)
        public void onClick() {
            switch (index) {
                case 0:
                    if (onGalleryClicked != null)
                        onGalleryClicked.onGalleryClicked();
                    break;
                case 1:
                    if (onGalleryClicked != null)
                        onGalleryClicked.onCameraClicked();
                    break;
                default:
                    if (onGalleryClicked != null)
                        onGalleryClicked.onPicClicked(imageIds.get(index - 2), index - 2);
                    break;
            }
        }
    }

    private class Divider extends RecyclerView.ViewHolder {

        Divider(View itemView) {
            super(itemView);
        }
    }
}
