package com.timby.vknotes.adapters.listeners;

/**
 * Created by andre on 10.09.17.
 */

public interface OnGalleryClicked {

    void onGalleryClicked();

    void onCameraClicked();

    void onPicClicked(int imageId, int index);

}
