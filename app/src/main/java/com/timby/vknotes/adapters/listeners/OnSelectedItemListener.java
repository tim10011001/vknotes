package com.timby.vknotes.adapters.listeners;

/**
 * Created by andre on 10.09.17.
 */

public interface OnSelectedItemListener {
    void onItemSelected(int itemIndex,int itemPos);
}
