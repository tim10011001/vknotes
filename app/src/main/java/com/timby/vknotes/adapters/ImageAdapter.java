package com.timby.vknotes.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.timby.vknotes.R;
import com.timby.vknotes.adapters.listeners.OnSelectedItemListener;
import com.timby.vknotes.utils.UIUtils;

import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andre on 08.09.17.
 */

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final float FOCUS_PADDING_DP = 4;

    private final int picFocusPadding;
    private final Context context;
    private final boolean fromAssets;
    private final String[] iconPaths;
    private final int[] iconResIds;
    private final boolean isCheckable;

    private int startPx = 0, dividePx = 0, endPx = 0, orientation = 0;

    private int selectedIndex = -1, selectedPos = -1;

    private OnSelectedItemListener onSelectedItemListener;


    public ImageAdapter(Context contexts, String[] iconPaths, boolean fromAssets, boolean isCheckable) {
        this.context = contexts;
        this.iconPaths = iconPaths;
        this.fromAssets = fromAssets;
        this.iconResIds = null;
        this.isCheckable = isCheckable;
        this.picFocusPadding = UIUtils.dpToPx(context, FOCUS_PADDING_DP);

        this.selectedIndex = isCheckable ? 0 : -1;
        this.selectedPos = isCheckable ? 1 : -1;

    }

    public ImageAdapter(Context context, int[] resIds, boolean isCheckable) {
        this.context = context;
        this.iconResIds = resIds;
        this.fromAssets = false;
        this.iconPaths = null;
        this.isCheckable = isCheckable;
        this.picFocusPadding = UIUtils.dpToPx(context, FOCUS_PADDING_DP);

        this.selectedIndex = isCheckable ? 0 : -1;
        this.selectedPos = isCheckable ? 1 : -1;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public OnSelectedItemListener getOnSelectedItemListener() {
        return onSelectedItemListener;
    }

    public void setOnSelectedItemListener(OnSelectedItemListener onSelectedItemListener) {
        this.onSelectedItemListener = onSelectedItemListener;
    }

    public void setDividers(int startPx, int dividePx, int endPx, int orientation) {
        this.startPx = startPx;
        this.dividePx = dividePx;
        this.endPx = endPx;
        this.orientation = orientation;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return 0;
        return position != getItemCount() - 1 ? position % 2 + 2 : 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int divide = 0;
        switch (viewType) {
            case 0:
                divide = startPx;
                break;
            case 1:
                divide = endPx;
                break;
            case 2:
                divide = dividePx;
                break;
            default:
                return new IconItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false));
        }


        View view = new View(context);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(0, 0);
        if (orientation == LinearLayoutManager.HORIZONTAL)
            layoutParams.width = divide;
        else
            layoutParams.height = divide;
        view.setLayoutParams(layoutParams);

        return new Divider(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 3:
                ((IconItem) holder).bind(position / 2, position);
        }
    }

    @Override
    public int getItemCount() {
        int len = iconResIds == null ? iconPaths.length : iconResIds.length;
        return len * 2 + 1;
    }

    class IconItem extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_item)
        ImageView ivImageView;

        private View itemView;
        private int pos;
        private int index;

        IconItem(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }


        void bind(int index, int pos) {
            this.pos = pos;
            this.index = index;
            final Drawable pic = getPic(index);

            if (index == selectedIndex) {
                final Drawable drawableClipperFocus = context.getResources().getDrawable(R.drawable.cliper_cornes_focus);
                LayerDrawable drawables = new LayerDrawable(new Drawable[]{pic, drawableClipperFocus});
                drawables.setLayerInset(0, picFocusPadding, picFocusPadding, picFocusPadding, picFocusPadding);
                ivImageView.setImageDrawable(drawables);
                return;
            }
            final Drawable drawableClipper = context.getResources().getDrawable(R.drawable.cliper_corners);
            LayerDrawable drawables = new LayerDrawable(new Drawable[]{pic, drawableClipper});
            ivImageView.setImageDrawable(drawables);
        }

        @OnClick(R.id.iv_item)
        void onClicked() {
            if (!isCheckable) return;
            selectedIndex = index;

            notifyItemChanged(selectedPos);
            notifyItemChanged(pos);
            selectedPos = pos;

            if (onSelectedItemListener != null)
                onSelectedItemListener.onItemSelected(index, pos);

        }

        private Drawable getPic(int index) {
            if (iconPaths != null) {
                InputStream stream;
                try {
                    stream = fromAssets ?
                            context.getAssets().open(iconPaths[index]) :
                            context.openFileInput(iconPaths[index]);
                    return new BitmapDrawable(BitmapFactory.decodeStream(stream));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return context.getResources().getDrawable(iconResIds[index]);
        }
    }

    private class Divider extends RecyclerView.ViewHolder {

        Divider(View itemView) {
            super(itemView);
        }
    }


}
