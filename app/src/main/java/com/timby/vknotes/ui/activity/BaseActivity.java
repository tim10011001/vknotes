package com.timby.vknotes.ui.activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

/**
 *
 * Created by andre on 13.09.17.
 */

public class BaseActivity extends AppCompatActivity {

    View mDecorView;
    private boolean softKeyboardVisible = false;
    private boolean keyboardListenersAttached = false;

    protected void onSoftKeyboard(boolean isVisible) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {
                }
            }
        });
        attachKeyboardListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (keyboardListenersAttached) {
            mDecorView.getViewTreeObserver().removeGlobalOnLayoutListener(keyboardLayoutListener);
        }
    }

    protected void attachKeyboardListeners() {
        if (keyboardListenersAttached) {
            return;
        }
        mDecorView.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);

        keyboardListenersAttached = true;
    }


    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

        @Override
        public void onGlobalLayout() {
            int navigationBarHeight = 0;
            int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
            }
            // status bar height
            int statusBarHeight = 0;
            resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }

            // display window size for the app layout
            Rect rect = new Rect();
            getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);

            // screen height - (user app height + status + nav) ..... if non-zero, then there is a soft keyboard
            int keyboardHeight = mDecorView.getRootView().getHeight() - (statusBarHeight + navigationBarHeight + rect.height());

            boolean l = softKeyboardVisible;
            if (keyboardHeight <= 0) {
                if (softKeyboardVisible)
                    onSoftKeyboard(softKeyboardVisible = false);
            } else {
                if (!softKeyboardVisible)
                    onSoftKeyboard(softKeyboardVisible = true);
            }
        }
    };
}
