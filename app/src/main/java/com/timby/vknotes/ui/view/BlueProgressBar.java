package com.timby.vknotes.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.timby.vknotes.R;

/**
 * Created by andre on 11.09.17.
 */

public class BlueProgressBar extends ProgressBar {


    public BlueProgressBar(Context context) {
        super(context);
    }

    public BlueProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlueProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.blue),
                android.graphics.PorterDuff.Mode.MULTIPLY);
    }

}
