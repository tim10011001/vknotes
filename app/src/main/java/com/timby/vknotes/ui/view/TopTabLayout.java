package com.timby.vknotes.ui.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by andre on 07.09.17.
 */

public class TopTabLayout extends TabLayout {
    public TopTabLayout(Context context) {
        this(context, null);
    }

    public TopTabLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TopTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setScaleY(-1);
    }

    @Override
    public void addTab(@NonNull Tab tab) {
        super.addTab(tab);

        int len = ((ViewGroup) this.getChildAt(0)).getChildCount();
        for (int i = 0; i < len; i++) {
            ((ViewGroup) ((ViewGroup) this.getChildAt(0)).getChildAt(i)).getChildAt(1).setScaleY(-1);
        }
    }
}
