package com.timby.vknotes.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.timby.vknotes.R;
import com.timby.vknotes.model.StickerPos;
import com.timby.vknotes.ui.view.gesture.DragGestureDetector;
import com.timby.vknotes.ui.view.gesture.RotationGestureDetector;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andre on 14.09.17.
 */

public class StickersField extends RelativeLayout {

    private static final int ANIM_DURACTION = 300;

    private List<StickerPos> stickers = new ArrayList<>();
    private List<View> vStickers = new ArrayList<>();

    private int dragIndex = -1;
    private View dragView = null;
    private View defTouchView = null;
    private boolean dragViewAnim = false;
    private FloatingActionButton ivFabTrash;

    private ScaleGestureDetector scaleGestureDetector;
    private RotationGestureDetector rotationGestureDetector;
    private DragGestureDetector dragGestureDetector;
    private GestureDetector simpleGestureClickDetector;

    private OnTouchListener stickerOnTouchListener = (v, event) -> {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                dragView = v;
                dragIndex = vStickers.indexOf(dragView);
        }
        return false;
    };

    private PosCalculator posCalculator = new PosCalculator();

    public StickersField(Context context) {
        super(context);
        init();
    }

    public StickersField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public StickersField(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setWillNotDraw(false);
        scaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        rotationGestureDetector = new RotationGestureDetector(new RotationListener());
        dragGestureDetector = new DragGestureDetector(new DragListener());
        simpleGestureClickDetector = new GestureDetector(new SingleTapConfirm());
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ivFabTrash = (FloatingActionButton) findViewById(R.id.fab_trash);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        new Handler().postDelayed(this::notifyDataChange, 30);
        notifyDataChange();
    }

    public List<StickerPos> getStickers() {
        return stickers;
    }

    public void setStickers(List<StickerPos> stickers) {
        this.stickers = stickers;
        notifyDataChange();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean isTouch = onTouch(event);
        if (isTouch) {
            if (!ivFabTrash.isShown())
                ivFabTrash.show();
        } else if (ivFabTrash.isShown())
            ivFabTrash.hide();
        return true;
    }


    private boolean onTouch(MotionEvent event) {
        if (simpleGestureClickDetector.onTouchEvent(event)) {
            dragView = null;
            defTouchView.setEnabled(true);
            return defTouchView.callOnClick();
        }
        if (dragView == null)
            return false;

        if (checkTouchInside(event, ivFabTrash)) {
            if (!dragViewAnim) {
                dragViewAnim = true;
                dragView.animate()
                        .setDuration(ANIM_DURACTION)
                        .translationX(ivFabTrash.getX())
                        .translationY(ivFabTrash.getY())
                        .alpha(0)
                        .scaleX(0)
                        .scaleY(0)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                dragViewAnim = false;
                            }
                        })
                        .start();
            }

            ivFabTrash.setImageResource(R.drawable.ic_fab_trash_released);
            if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                deleteDragView();
                return false;
            }

        } else {
            if (!dragViewAnim && dragView.getAlpha() == 0) {
                dragViewAnim = true;
                StickerPos stickerPos = stickers.get(dragIndex);
                dragView.animate()
                        .setDuration(ANIM_DURACTION)
                        .alpha(1)
                        .scaleX(stickerPos.getScale())
                        .scaleY(stickerPos.getScale())
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                dragViewAnim = false;
                            }
                        })
                        .start();
            }

            ivFabTrash.setImageResource(R.drawable.ic_fab_trash);
        }

        defTouchView.setEnabled(false);
        scaleGestureDetector.onTouchEvent(event);
        rotationGestureDetector.onTouchEvent(event);
        dragGestureDetector.onTouchEvent(event);
        return event.getActionMasked() != MotionEvent.ACTION_UP && event.getActionMasked() != MotionEvent.ACTION_CANCEL;
    }

    public void notifyDataChange() {
        for (int i = 0; i < vStickers.size(); i++) {
            removeView(vStickers.get(i));
        }
        vStickers.clear();

        for (int i = 0; i < stickers.size(); i++) {
            StickerPos stickerPos = stickers.get(i);
            String stickerPath = stickerPos.getStickerPath();
            InputStream stickerStream = null;
            try {
                stickerStream = getContext().getAssets().open(stickerPath);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ImageView stickerView = new ImageView(getContext());
            stickerView.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeStream(stickerStream)));
            PointF pointF = posCalculator.getLocal(new PointF(stickerPos.getX(), stickerPos.getY()));

            stickerView.setX(pointF.x);
            stickerView.setY(pointF.y);
            stickerView.setRotation(stickerPos.getAngle());
            stickerView.setScaleX(stickerPos.getScale());
            stickerView.setScaleY(stickerPos.getScale());
            stickerView.setOnTouchListener(stickerOnTouchListener);

            addView(stickerView, 1);
            vStickers.add(stickerView);
        }
    }

    public View getDefTouchView() {
        return defTouchView;
    }

    public void setDefTouchView(View defTouchView) {
        this.defTouchView = defTouchView;
    }

    private void deleteDragView() {
        stickers.remove(dragIndex);
        vStickers.remove(dragView);
        removeView(dragView);
        dragView = null;
        dragIndex = -1;

    }

    private boolean checkTouchInside(MotionEvent event, View view) {
        Rect editTextRect = new Rect();
        view.getHitRect(editTextRect);
        return editTextRect.contains((int) event.getX(), (int) event.getY());
    }

    private class PosCalculator {

        /**
         * @param pointF
         * @return range (0,w) or (o,h)
         */
        PointF getLocal(@NonNull PointF pointF) {
            float w = getWidth();
            float h = getHeight();

            return new PointF(w * (1f + pointF.x) / 2f, h * (1f + pointF.y) / 2f);
        }

        /**
         * @param pointF
         * @return range (-1,1)
         */
        PointF getGlobal(@NonNull PointF pointF) {
            float w = getWidth();
            float h = getHeight();

            return new PointF(pointF.x / w * 2f - 1f, pointF.y / h * 2f - 1f);
        }

        PointF getViewPosGlobal(View view) {
            float x = view.getX() + view.getWidth() / 2f;
            float y = view.getY() + view.getHeight() / 2f;
            return getGlobal(new PointF(x, y));
        }

        void setViewPosLocal(View view, PointF global) {
            PointF local = getLocal(global);
            view.setX(local.x - view.getWidth());
            view.setY(local.y - view.getHeight());
        }

    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));

            StickerPos stickerPos = stickers.get(dragIndex);
            float scale = stickerPos.getScale() * scaleFactor;
            stickerPos.setScale(scale);
            dragView.setScaleY(scale);
            dragView.setScaleX(scale);

            return true;
        }
    }

    private class RotationListener implements RotationGestureDetector.OnRotationGestureListener {

        float startAngle = 0;

        @Override
        public void onRotation(RotationGestureDetector rotationDetector) {
            float angle = startAngle - rotationDetector.getAngle();

            StickerPos stickerPos = stickers.get(dragIndex);
            stickerPos.setAngle(angle);
            dragView.setRotation(angle);
        }

        @Override
        public void onRotationStart(RotationGestureDetector rotationDetector) {
            startAngle = stickers.get(dragIndex).getAngle();
        }
    }

    private class DragListener implements DragGestureDetector.OnDragGestureListener {

        private float startX = 0, startY = 0;

        @Override
        public void onDrag(DragGestureDetector rotationDetector) {
            PointF dragF = rotationDetector.getDrag();

            dragView.setX(dragF.x + startX);
            dragView.setY(dragF.y + startY);

            PointF global = posCalculator.getGlobal(new PointF(dragView.getX(), dragView.getY()));
            StickerPos stickerPos = stickers.get(dragIndex);
            stickerPos.setX(global.x);
            stickerPos.setY(global.y);

        }

        @Override
        public void onDragStart(DragGestureDetector rotationDetector) {
            startX = dragView.getX();
            startY = dragView.getY();
        }
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            defTouchView.onTouchEvent(e);
            return true;
        }
    }
}
