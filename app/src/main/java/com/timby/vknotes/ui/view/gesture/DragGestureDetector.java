package com.timby.vknotes.ui.view.gesture;

import android.graphics.PointF;
import android.view.MotionEvent;

/**
 * https://stackoverflow.com/questions/10682019/android-two-finger-rotation
 * Created by andre on 19.09.17.
 */

public class DragGestureDetector {
    private float dragX = -1, dragY = -1;
    private float dX = -1, dY = 0;

    private OnDragGestureListener mListener;

    public PointF getDrag() {
        return new PointF(dragX, dragY);
    }

    public DragGestureDetector(OnDragGestureListener listener) {
        mListener = listener;
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                dX = -1;
                return true;
            }
            case MotionEvent.ACTION_MOVE:
                if (dX == -1) {
                    float ccX = 0, ccY = 0;
                    for (int i = 0; i < event.getPointerCount(); i++) {
                        ccX += event.getX(i);
                        ccY += event.getY();
                    }
                    ccX /= (float) event.getPointerCount();
                    ccY /= (float) event.getPointerCount();

                    dX = ccX;
                    dY = ccY;

                    dragX = 0;
                    dragY = 0;

                    if (mListener != null) {
                        mListener.onDragStart(this);
                    }

                    return true;
                }


                float ccX = 0, ccY = 0;
                for (int i = 0; i < event.getPointerCount(); i++) {
                    ccX += event.getX(i);
                    ccY += event.getY();
                }
                ccX /= (float) event.getPointerCount();
                ccY /= (float) event.getPointerCount();


                dragX = ccX - dX;
                dragY = ccY - dY;

                if (mListener != null) {
                    mListener.onDrag(this);
                }

                break;
        }
        return true;
    }

    public interface OnDragGestureListener {
        void onDrag(DragGestureDetector rotationDetector);

        void onDragStart(DragGestureDetector rotationDetector);
    }
}