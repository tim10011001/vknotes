package com.timby.vknotes.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.evernote.android.state.State;
import com.evernote.android.state.StateSaver;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.timby.vknotes.R;
import com.timby.vknotes.adapters.GalleryAdapter;
import com.timby.vknotes.adapters.ImageAdapter;
import com.timby.vknotes.adapters.StickersAdapter;
import com.timby.vknotes.adapters.listeners.OnGalleryClicked;
import com.timby.vknotes.adapters.listeners.OnSelectedItemListener;
import com.timby.vknotes.adapters.listeners.OnStickerPressedListener;
import com.timby.vknotes.model.StickerPos;
import com.timby.vknotes.ui.view.StickersField;
import com.timby.vknotes.utils.AssetsUtils;
import com.timby.vknotes.utils.ColorUtils;
import com.timby.vknotes.utils.FilesUtils;
import com.timby.vknotes.utils.StaticValues;
import com.timby.vknotes.utils.UIUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoteActivity extends BaseActivity implements OnStickerPressedListener {

    private static final int PERMISSION_REQUEST_GALLERY = 705;
    private static final int PERMISSION_REQUEST_CAMERA = 7032;

    private static final int PICK_PICTURE = 239;
    private static final int REQUEST_TAKE_PHOTO = 914;

    private static final int[] THUMBNAILS = new int[]{R.drawable.thumb_empty, R.drawable.thumb_blue, R.drawable.thumb_green,
            R.drawable.thumb_orange, R.drawable.thumb_cherry, R.drawable.thumb_periwinkle,
            R.drawable.thumb_beach, R.drawable.thumb_stars, R.drawable.ic_toolbar_new};

    private static final int[] TEXT_BACKGROUND_COLOR = new int[]{0x00000000, 0xaaffffff, 0xffffffff};

    private static final String TAG = NoteActivity.class.getSimpleName();


    @Bind(R.id.tab_layout)
    TabLayout tabLayout;
    @Bind(R.id.sliding_panel)
    SlidingUpPanelLayout slidingPaneLayout;
    @Bind(R.id.ll_main_panel)
    LinearLayout llMainPanel;

    @Bind(R.id.msf_draw_field)
    StickersField msfStickersDrawField;
    @Bind(R.id.iv_draw_image)
    ImageView ivDrawImage;
    @Bind(R.id.edit_text)
    EditText editText;
    @Bind(R.id.bt_send_node)
    Button btSend;

    @Bind(R.id.rl_bottom_control)
    RelativeLayout rlBottomControl;
    @Bind(R.id.rl_pop_panel)
    RelativeLayout rlPopPanel;
    @Bind(R.id.rv_stickers)
    RecyclerView rvStickers;
    @Bind(R.id.rv_backgrounds)
    RecyclerView rvBackgrounds;
    @Bind(R.id.rv_photos)
    RecyclerView rvPhotos;

    private List<Integer> images;

    @State
    String curImageUri;
    @State
    String curImagePath;
    @State
    int curImageRes = -1;
    @State
    String curText;
    @State
    ArrayList<StickerPos> stickers = new ArrayList<>();
    @State
    String mCurrentPhotoPath;
    @State
    ImageView.ScaleType scaleType = ImageView.ScaleType.CENTER_CROP;
    @State
    int curTextBackgroundColor = TEXT_BACKGROUND_COLOR[0];


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            curText = editText.getText().toString();
            checkSendBt();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        ButterKnife.bind(this);
        StateSaver.restoreInstanceState(this, savedInstanceState);

        slidingPaneLayout.setPanelSlideListener(new SlidingUpPanelLayout.SimplePanelSlideListener() {
            @Override
            public void onPanelCollapsed(View panel) {
                super.onPanelCollapsed(panel);
                llMainPanel.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

            }

            @Override
            public void onPanelExpanded(View panel) {
                super.onPanelExpanded(panel);
            }
        });

        slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        slidingPaneLayout.setScrollableView(rvStickers);
        rvStickers.setLayoutManager(new GridLayoutManager(this, 4));
        rvStickers.setAdapter(new StickersAdapter(this, AssetsUtils.getAssetsInFolder(this, AssetsUtils.STICKERS_ASSETS_FOLDER), this));

        rvBackgrounds.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        ImageAdapter thumbAdapter = new ImageAdapter(this, THUMBNAILS, true);
        thumbAdapter.setDividers(getResources().getDimensionPixelOffset(R.dimen.thumb_divide_start),
                getResources().getDimensionPixelOffset(R.dimen.thumb_divide),
                getResources().getDimensionPixelOffset(R.dimen.thumb_divide_end),
                LinearLayoutManager.HORIZONTAL);
        thumbAdapter.setOnSelectedItemListener(new OnSelectedThumb());
        rvBackgrounds.setAdapter(thumbAdapter);


        rvPhotos.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.HORIZONTAL, false));

        editText.setText(curText);
        updateCurImage();
        setTextBackgroundColor(curTextBackgroundColor);
        checkSendBt();

        msfStickersDrawField.setDefTouchView(editText);
        msfStickersDrawField.setStickers(stickers);


    }

    @Override
    protected void onResume() {
        super.onResume();
        UIUtils.hideKeyboard(this, null);
        editText.addTextChangedListener(textWatcher);
    }

    @Override
    protected void onPause() {
        super.onPause();
        editText.removeTextChangedListener(textWatcher);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        curText = editText.getText().toString();
        StateSaver.saveInstanceState(this, outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_GALLERY: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    rvPhotos.setVisibility(View.VISIBLE);
                    images = FilesUtils.getImages(NoteActivity.this);
                    GalleryAdapter galleryAdapter = new GalleryAdapter(NoteActivity.this, images);
                    rvPhotos.setAdapter(galleryAdapter);
                    galleryAdapter.setOnGalleryClicked(new OnGalleryListener());
                } else {
                    Toast.makeText(this, getResources().getText(R.string.no_permissions), Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_PICTURE:
                if (resultCode == RESULT_OK && data != null) {
                    setCurImage(-1, null, data.getData(), ImageView.ScaleType.FIT_CENTER);

                }
                break;
            case REQUEST_TAKE_PHOTO:
                if (resultCode == RESULT_OK && mCurrentPhotoPath != null) {
                    setCurImage(-1, mCurrentPhotoPath, null, ImageView.ScaleType.FIT_CENTER);
                    galleryAddPic();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSoftKeyboard(boolean isVisible) {
        super.onSoftKeyboard(isVisible);
        if (isVisible)
            rvPhotos.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (slidingPaneLayout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
            slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return;
        }

        if (rvPhotos.getVisibility() == View.VISIBLE) {
            rvPhotos.setVisibility(View.GONE);
            return;
        }

        super.onBackPressed();

    }

    @OnClick(R.id.bt_open_stickers_panel)
    public void onBtStickers() {
        rlPopPanel.getLayoutParams().height = Math.round(UIUtils.getWindowHeight(this) * 2f / 3f);
        UIUtils.hideKeyboard(this, () -> slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED));
    }

    @OnClick(R.id.ibt_text_style)
    public void onBtTextStyle() {
        int i = 0;
        for (; i < TEXT_BACKGROUND_COLOR.length && TEXT_BACKGROUND_COLOR[i] != curTextBackgroundColor; i++)
            ;
        i++;
        if (i >= TEXT_BACKGROUND_COLOR.length)
            i = 0;
        setTextBackgroundColor(TEXT_BACKGROUND_COLOR[i]);
    }

    @OnClick(R.id.bt_send_node)
    public void onBtSend() {
        editText.clearFocus();

        UIUtils.hideKeyboard(this, () -> {
            editText.setCursorVisible(false);
            Bitmap b = Bitmap.createBitmap(msfStickersDrawField.getWidth(), msfStickersDrawField.getHeight(),
                    Bitmap.Config.ARGB_8888);

            Canvas c = new Canvas(b);

            msfStickersDrawField.draw(c);

            StaticValues.publishBitmap = b;

            startActivity(new Intent(this, PublishActivity.class));
            editText.setCursorVisible(true);

        });

    }

    @Override
    public void onStickerPressed(String assetPath) {
        StickerPos stickerPos = StickerPos.pasteToFreePlace(assetPath, stickers);
        stickers.add(stickerPos);
        msfStickersDrawField.setStickers(stickers);

        slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
    }

    private void updateCurImage() {
        setCurImage(curImageRes, curImagePath, curImageUri != null ? Uri.parse(curImageUri) : null, scaleType);
    }

    private void setCurImage(int res, String pathImage, Uri uriImage, ImageView.ScaleType imageScaleType) {
        if (uriImage != null) {
            this.curImagePath = null;
            this.curImageUri = uriImage.toString();
            this.curImageRes = -1;
            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    ivDrawImage.setScaleType(NoteActivity.this.scaleType = imageScaleType);
                    ivDrawImage.setImageBitmap(bitmap);
                    int backColor = ColorUtils.getAverageColor(ivDrawImage);
                    editText.setTextColor(ColorUtils.getContrastTextColor(NoteActivity.this, backColor, curTextBackgroundColor));
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            ivDrawImage.setTag(target);
            Picasso.with(NoteActivity.this)
                    .load(uriImage)
                    .into(target);

            return;
        }
        if (pathImage != null) {
            this.curImagePath = pathImage;
            this.curImageUri = null;
            this.curImageRes = -1;
            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    ivDrawImage.setScaleType(NoteActivity.this.scaleType = imageScaleType);
                    ivDrawImage.setImageBitmap(bitmap);
                    int backColor = ColorUtils.getAverageColor(ivDrawImage);
                    editText.setTextColor(ColorUtils.getContrastTextColor(NoteActivity.this, backColor, curTextBackgroundColor));
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            ivDrawImage.setTag(target);
            Picasso.with(NoteActivity.this)
                    .load(new File(pathImage))
                    .into(target);
            return;
        }

        if (res != -1) {
            this.curImagePath = null;
            this.curImageUri = null;
            this.curImageRes = res;
            ivDrawImage.setScaleType(NoteActivity.this.scaleType = imageScaleType);
            ivDrawImage.setImageResource(res);
            int backColor = ColorUtils.getAverageColor(ivDrawImage);
            editText.setTextColor(ColorUtils.getContrastTextColor(NoteActivity.this, backColor, curTextBackgroundColor));
        }
    }

    private void setTextBackgroundColor(int textBackgroundColor) {
        this.curTextBackgroundColor = textBackgroundColor;
        GradientDrawable d = (GradientDrawable) getResources().getDrawable(R.drawable.text_back);
        d.setColor(textBackgroundColor);
        editText.setBackground(d);
        updateCurImage();
    }

    private void checkSendBt() {
        btSend.setEnabled(curText != null && !curText.isEmpty());
    }

    private boolean checkGalleryPermissions() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_GALLERY);
            return false;
        }
    }

    private boolean checkCameraPermissions() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    PERMISSION_REQUEST_CAMERA);
            return false;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.timby.vknotes.provider.CaptureFileProvider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


    private class OnSelectedThumb implements OnSelectedItemListener {
        @Override
        public void onItemSelected(int itemIndex, int itemPos) {

            final int finalItemIndex = itemIndex;
            UIUtils.hideKeyboard(NoteActivity.this, () -> {
                int localItemIndex = finalItemIndex;
                if (localItemIndex < THUMBNAILS.length - 1) {
                    rvPhotos.setVisibility(View.GONE);
                    return;
                }
                localItemIndex -= THUMBNAILS.length - 3;
                switch (localItemIndex) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        if (!checkGalleryPermissions()) break;
                        if (rvPhotos.getVisibility() != View.VISIBLE) {
                            rvPhotos.setVisibility(View.VISIBLE);

                            images = FilesUtils.getImages(NoteActivity.this);
                            GalleryAdapter galleryAdapter = new GalleryAdapter(NoteActivity.this, images);
                            rvPhotos.setAdapter(galleryAdapter);
                            galleryAdapter.setOnGalleryClicked(new OnGalleryListener());
                        } else {
                            rvPhotos.setVisibility(View.GONE);
                        }
                }

            });
            if (itemIndex < THUMBNAILS.length - 3) {
                setCurImage(THUMBNAILS[itemIndex], null, null, ImageView.ScaleType.CENTER_CROP);
                return;
            }
            itemIndex -= THUMBNAILS.length - 3;
            switch (itemIndex) {
                case 0:
                    setCurImage(R.drawable.composite_beach, null, null, ImageView.ScaleType.FIT_XY);
                    break;
                case 1:
                    setCurImage(R.drawable.bg_stars_center, null, null, ImageView.ScaleType.FIT_XY);
                    break;
                case 2:
                    break;
            }


        }
    }

    private class OnGalleryListener implements OnGalleryClicked {

        @Override
        public void onGalleryClicked() {
            if (!checkGalleryPermissions()) return;
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");
            startActivityForResult(getIntent, PICK_PICTURE);
        }

        @Override
        public void onCameraClicked() {
            if (!checkCameraPermissions()) return;
            dispatchTakePictureIntent();
        }

        @Override
        public void onPicClicked(int imageId, int index) {
            String filePath = FilesUtils.getImagePath(NoteActivity.this, imageId);
            setCurImage(-1, filePath, null, ImageView.ScaleType.FIT_CENTER);

        }
    }

}
