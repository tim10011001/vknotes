package com.timby.vknotes.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.timby.vknotes.R;
import com.timby.vknotes.network.IPoster;
import com.timby.vknotes.network.Poster;
import com.timby.vknotes.utils.StaticValues;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andre on 11.09.17.
 */

public class PublishActivity extends AppCompatActivity {

    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.iv_succes)
    ImageView ivSuccess;
    @Bind(R.id.tv_publish_state)
    TextView tvPusblishState;

    @Bind(R.id.bt_action)
    Button btAction;

    WorkMode workMode = WorkMode.PUSHING;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);
        ButterKnife.bind(this);
        push();
    }

    @OnClick(R.id.bt_action)
    public void onAction() {
        switch (workMode) {
            case PUSHING:
                finish();
                break;
            case SUCCESS:
                Intent intent = new Intent(this, NoteActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case ERROR:
                push();
                break;
        }

    }

    private void push() {
        new Poster().post(StaticValues.publishBitmap, new IPoster.RequestCallback() {
            @Override
            public void onSuccess() {
                onPushSuccess();
            }

            @Override
            public void onError(Throwable exception) {

            }
        });

        workMode = WorkMode.PUSHING;
        progressBar.setVisibility(View.VISIBLE);
        ivSuccess.setVisibility(View.INVISIBLE);
        tvPusblishState.setText(getResources().getString(R.string.publishing));

        btAction.setBackgroundResource(R.drawable.selector_cancel_bt);
        btAction.setTextColor(getResources().getColor(R.color.cornflower_blue_two));
        btAction.setText(getString(R.string.cancel));
    }

    private void onPushSuccess() {
        workMode = WorkMode.SUCCESS;

        progressBar.setVisibility(View.INVISIBLE);
        ivSuccess.setVisibility(View.VISIBLE);
        tvPusblishState.setText(getResources().getString(R.string.published));

        btAction.setBackgroundResource(R.drawable.selector_send_bt);
        btAction.setTextColor(getResources().getColor(R.color.white));
        btAction.setText(getString(R.string.send_also));
    }

    private void onPushError() {
        workMode = WorkMode.ERROR;

        progressBar.setVisibility(View.INVISIBLE);
        tvPusblishState.setText(getResources().getString(R.string.publish_error));

        btAction.setBackgroundResource(R.drawable.selector_send_bt);
        btAction.setTextColor(getResources().getColor(R.color.white));
        btAction.setText(getString(R.string.repeat_send));
    }

    private enum WorkMode {PUSHING, SUCCESS, ERROR}
}
